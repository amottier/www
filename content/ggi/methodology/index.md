---
title: "GGI: Methodology"
date: 2021-10-07T10:00:00-04:00
show_featured_footer: false
layout: "single"
---

----

**Table of contents**

{{< table_of_contents >}}

----

# Methodology

Implementing the OSS Good Governance methodology is ultimately a consequential and impactful initiative. It involves several categories of company people, services, and processes, from everyday practices to HR management and from developers to C-level executives. There really is no silver bullet mechanism to implement open source good governance. Different types of organisation and company cultures and situations will demand different approaches to open source governance. For each organisation, there will be different constraints and expectations, leading to different paths and ways of managing the programme.

With this in mind the Good Governance Initiative provides a generic blueprint of activities that can be tailored to an organisation's own domain, culture and requirements. While the blueprint claims to be comprehensive, the methodology can be implemented progressively. It is possible to bootstrap the program by simply selecting the most relevant Goals and Activities in one's specific context. The idea is to build a first-draft roadmap to help set up the local initiative.

Besides this framework, we also highly recommend getting in touch with peers through an established network like the European [OSPO Alliance](https://ospo-alliance.org) initiative, or other like-minded initiatives from the TODO group or OSPO++. What is important is to be able to exchange with people running a similar initiative, and share the issues encountered and the solutions that exist.

## Setting the Stage

Given the ambition of the good governance methodology and its potentially broad impact, it is important to communicate with a variety of people within an organisation. It would be appropriate to onboard them to establish an initial set of realistic expectations and requirements to get off to a good start, attract interest and support. A good idea is to publish the Customised Activity Scorecards on the organisation's collaborative platform so they can be used to communicate with stakeholders. Some hints:

* Identify key stakeholders, make them agree on a set of primary objectives. Engage them in the success of the initiative as a part of their own agenda.
* Get initial buy-in, agree on the steps and pace, and set up regular checks to inform them of progress.
* Make sure they understand the benefits of what can be achieved and what it involves: expected improvement should be clear and outcome visible.
* Establish a first diagnostic or state of the art of open source in the candidate organisation.
  Outcome: a document describing what this program will achieve, where the organisation stands and where it aims to go.

## Workflow

As modern software practitioners, we like agile-like methods that define small and safe increments, since it is good practice to reassess the situation regularly and to provide meaningful minimum intermediate results.

In the context of a live OSPO program this is highly relevant, as many side aspects will change over time, from the organisation's strategy and response to open source to people's availability and engagement. Periodic reassessment and iteration also allows for adaptation to ongoing programme acceptance, better tracking of current trends and opportunities, and small incremental benefits to stakeholders and the organisation as a whole.

Ideally, the methodology could be implemented in five phases as follows:

1. **Discovery** Understanding key concepts, taking ownership of the methodology, aligning goals expectations.
1. **Customisation** Adapting Activity description and opportunity assessment to organisation specifics.
1. **Prioritisation** Identifying objectives and key results, tasks and tools, scheduling milestones and drafting timeline.
1. **Activation** Finalising Scorecard, budget, assignments, document tasks on issue manager.
1. **Iteration** Assessing and scoring results, highlighting issues, improving, adjusting. Iterate every quarter or semester.

Preparing for the first program iteration:

* Identify a first set of tasks to work on, and prioritise them according to the needs (gaps to the desired state) and the timeline. Outcome: a list of tasks to work on during the iteration.
* Define a set of requirements and areas of improvement, communicate it to the stakeholders and end-users, and get their approval or commitment.
* Fill the scorecards for progress tracking. A template scorecard can be downloaded from the [GGI repository](https://gitlab.ow2.org/ggi/ggi/-/tree/main/resources/).

At the end of each iteration, do a retrospective and prepare for the next iteration:

* Communicate about the latest improvements.
* Assess where you are, if the targeted tasks have been completed, refine the roadmap accordingly.
* Check the remaining pain points or issues, ask for support from other actors or services if needed.
* Re-prioritise tasks according to the updated context.
* Define a new subset of tasks to execute.

## Manual setup: using Customised Activity Scorecards

A Customised Activity Scorecard is a form describing a Canonical Activity customised to the specifics of an organisation. Put together, the deck of Customised Activity Scorecards provides the roadmap for managing open source software.

`Please note, from early experience with the methodology, it takes up to one hour to adapt a Canonical Activity into an organisation's specific Customised Score Card.`

The Customised Activity Scorecard contains the following sections:

* **Title Disambiguation** First of all take a few minutes to develop an understanding of what the Activity might be about and its relevance, how it can fit in your overall OSS management journey.
* **Customised Description** Adapt the Activity to the specifics of the organisation, scoping. Define the scope of the Activity, the particular use case you will address.
* **Opportunity Assessment** Explain why it is relevant to undertake this activity, what needs it addresses. What are our pain points? What are the opportunities for progressing? What can be gained?
* **Objectives** Define a couple of crucial objectives for the Activity. Pain points to be fixed, progress opportunities, wishes. Identify key tasks. What we aim to achieve in this iteration.
* **Tools** Technologies, tools and products used in the Activity.
* **Operational Notes** Indications on approach, method, strategy to progress in this Activity.
* **Key Results** Define measurable, verifiable expected results. Choose results indicating progress with regard to the Objectives. Indicate KPIs here.
* **Progress and Score** Progress is, in %, the completion rate of the result; Score is the personal success rating.
* **Personal Assessment** For each result you can add a brief explanation and explain your personal satisfaction rate expressed in the Score.
* **Timeline** Indicate Start-End dates, Phasing tasks, Critical steps, Milestones.
* **Efforts** Evaluate requested time and material resources, internal and third-party. What are the efforts expected? How much will it cost? What resources do we need?
* **Assignees** Say who participates. Assign tasks or Activity leadership and responsibilities.
* **Issues** Identify key issues, foreseen difficulties, risks, roadblocks, uncertainties, points of attention, critical dependencies.
* **Status** Write here a synthetic assessment of how the Activity is doing: healthy? Delayed? Etc.
* **Overall Progress Rating** Your own high-level, management-oriented, synthetic Activity progress evaluation.

## Automatic setup: using the GGI Deployment feature

Starting with the Handbook 1.1 version, the GGI proposes [My GGI Board](https://gitlab.ow2.org/ggi/my-ggi-board), an automated tooling to deploy your own instance of the GGI as a GitLab project. The install process takes less than 10mn to set up, is fully documented, and provides a simple and reliable way to customise the activities, follow their execution as you make progress, and communicate the results to your stakeholders. A live example of the deployment can be seen in the [initative's GitLab](https://gitlab.ow2.org/ggi/my-ggi-board-test), with the auto-generated website available on [its GitLab pages](https://ggi.ow2.io/my-ggi-board-test/).

{{< grid/div isMarkdown="false" >}}
<img src="/images/ggi/ggi_deploy_activities.png" alt="GGI Deploy activities" class="img-responsive">
<br />
{{</ grid/div >}}

Here is a standard workflow to use the deployment feature:

1. Fork the My GGI Board to your own GitLab instance or project, and set it up following the instructions in the project's README: <https://gitlab.ow2.org/ggi/my-ggi-board>. This will:
  - Create all the activities as issues in the project.
  - Create a nice board to help you visualise and manage the activities.
  - Create a static website, served on your GitLab instance pages, with the information extracted from the activities.
  - Update the project's description with the proper links to the board of activities and your static website.

2. From there, you can start looking at the different activities and filling in the scorecard section.
  - The scorecard section is the electronic (and simplified) equivalent of the above-mentioned ODT scorecards. They are used to adapt the activity to your context, by listing the local resources, risks and opportunities, and defining custom objectives required to complete the activity.
  - If some activity does not apply to your context, then simply mark it as 'Not Selected', or close it.
  - This is a quite time-consuming process, but highly needed as it will help you, step-by-step, to define your own roadmap and plan.

3. When activities have been defined, you can start implementing your own OSPO. Select a few activities that you think are relevant to start with, and change their progress label from 'Not Started' to 'In Progress'. You can use GitLab features to help you organise the work (comments, assignees, etc.) or any other tool. It's easy to link to the activities, and there are plenty of great integrations available.

4. On a regular basis (weekly, monthly, depending on your timetable), assess and review the current activities and when they are completed, change the label from 'In Progress' to 'Done'. Select a few other and start again at step 3 until they are all completed.

The website proposes a quick overview of the current and past activities, and extracts the scorecard section of issues to display only the locally relevant information. When changes happen in the issues (activities) these are automatically updated in the generated website. Please note that the CI pipelines for the automatic generation of the website are automatically executed nightly, but you can easily launch them from the GitLab project's CI/CD section. The following picture shows the auto-generated website interface.

{{< grid/div isMarkdown="false" >}}
<img src="/images/ggi/ggi_deploy_website.png" alt="GGI Deploy website" class="img-responsive">
<br />
{{</ grid/div >}}

You can ask questions or get support for the deployment feature at our GitLab homepage, and we welcome feedback.

> GGI Deploy homepage: <https://gitlab.ow2.org/ggi/my-ggi-board>

## Enjoy

Communicate on your success and enjoy the peace of mind of a state of the art open source strategy!

The OSS Good Governance is a method to deploy a continuous improvement program, and as such it never ends. Nevertheless, it's important to highlight intermediate steps and appreciate the changes it yields, to make progress visible and share the results.

* Communicate with stakeholders and end-users to let them know the advantages and benefits that the initiative's effort brings.
* Foster sustainability of the program. Ensure that the best practices and lessons learned from the program are always applied and updated.
* Share your experience with your peers: provide feedback to the GGI working group and within your OSPO community of adoption, and share your approach.
