---
title: "OSPO Resources"
date: 2021-11-21T10:00:00-04:00
show_featured_footer: false
layout: "single"
---

This section contains resources and supporting material for the OSPO Alliance.


## OnRamp Presentations

- **September 16, 2022**
  - Camille Moulin and Benjamin Jean ([InnoCube](https://innocube.org/)) - Hermine project: Open source code and data for legal compliance \
  [Download slides](/resources/onramp_20220916/20220916_OSPO_OnRamp_Hermine_v03.pdf) 

<br />

- **June 17, 2022**
  - Lucian Balea (RTE) - Setting up an OSPO at a power utility \
  [Download slides](/resources/onramp_20220617/220617_OSPO_OnRamp_Building_an_OSPO_at_RTE.pdf) - [Watch video](https://peertube.xwiki.com/w/bDWqEvU5twosD2J1jpYj3f)

<br />

- **March 18, 2022**
  - Daniel Izquierdo (Bitergia) - Establishing metrics early as an integral part of your OSPO first steps (35mn) \
  [Download slides](/resources/onramp_20220318/20220318_OSPO_OnRamp_Establishing_metrics_OSPO_first_steps.pdf) - [Watch video](https://peertube.xwiki.com/w/dwt4WL7AXvBVnfyRgdfpaw)

<br />

- **February 18, 2022**
  - Carsten Emde (OSADL) - How to facilitate Open Source software licensing and get it right? (35mn)\
    [Download slides](/resources/onramp_20220218/20220218_OSPO_OnRamp_OSADL_How-to-facilitate-Open-Source-software-licensing-and-get-it-right.pdf) - [Watch video](https://bbb.osadl.org/playback/presentation/2.3/d0de8b8cf7435dfa4ec3a9bbea43caea79fba6b6-1645177848413)

<br />

- **January 14, 2021**
  - Henrik Plate (SAP Security Research) - [Manage Log4Shell and other open-source vulnerabilities with Eclipse Steady](/resources/onramp_20220114/20220114_OSPO_Alliance_Log4Shell.pdf) (35min).

<br />

- **December 10, 2021**
  - Michael Plagge (Eclipse Foundation) - [Intro into the OSPO OnRamp concept - Call for active participation](/resources/onramp_20211210/20211210_OSPO_OnRamp.pdf) (10min).
  - Michael Picht (SAP) - [The SAP OSPO](/resources/onramp_20211210/20211210_ext_ospo_onramp_sap_ospo_overview.pdf) (25min).
  - Cédric Thomas (OW2) - [The OW2 OSS Good Governance initiative, an OSPO implementation blueprint](/resources/onramp_20211210/20211210_onramp-ggi-introduction-cedricthomas-CC-BY.pdf) (25min).


## GGI Presentations

* **Generic Presentations**
  - [GGI - an OSPO implementation blueprint](https://www.ow2.org/download/OSS_Governance/WebHome/2111-GGi-Introduction-CedricThomas.pdf?rev=1.1) - 32 slides
  - [GGI - OSS governance with a European perspective](https://www.ow2.org/download/Events/FOSDEM_2021/2102-FOSDEM-OW2-GGI-Intro.pdf) - 15 slides

<br />

* **Watch videos**
  - [FOSDEM 2021](https://www.youtube.com/watch?v=abQzK8gkZJU) (5 min)
  - [BlueHats Workshop 2021](https://www.dailymotion.com/video/x82vcud) (1 hour, French)
  - [Rete Italiana Open Source Week 2021](https://www.youtube.com/embed/I8ZZ52btxyY?hd=1&vq=hd1080&rel=0) (23 min)
  - [OW2con'20](https://bittube.video/videos/embed/cc071e1f-3895-45df-9bc6-9e4bb4f6aaad) (20 min) - [Download slides](https://www.slideshare.net/slideshow/embed_code/236093257) (29 slides)


## Curated resources

This section lists relevant resources submitted by our members.

* A paper published by [OpenForumEurope](https://openforumeurope.org) at the Digital Assembly, an event co-hosted by the French Government and the European Commission, on the 20th of June 2022:
  - [The OSPO: A New Tool for Digital Government](https://openforumeurope.org/publications/the-ospo-a-new-tool-for-digital-government/) (CC-BY-SA).
* A series of articles about SAP's corporate OSPO, by Michael Picht:
  - [Managing Open Source Software with an Open Source Program Office](https://blogs.sap.com/2021/09/23/managing-open-source-software-with-an-open-source-program-office/),
  - [How SAP Manages Open Source Software with an Open Source Program Office](https://blogs.sap.com/2021/10/28/how-sap-manages-open-source-software-with-an-open-source-program-office/).
