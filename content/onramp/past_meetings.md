---
title: "OSPO OnRamp past meetings"
date: 2021-11-21T10:00:00-04:00
show_featured_footer: false
layout: "single"
---

The idea of the OSPO OnRamp meeting series is to provide a low-threshold entry point for organisations who want to exchange and learn about the basics on how to set up an OpenSource Program Office and get started into open source.

Please check the [main OnRamp page](/onramp/) for more information about the next meetings.

The meeting are scheduled for every second Friday of the month from 10:30-12:00.

## Past meetings

(*) These parts have not been recorded.

**Friday, September 16th, 10:30-12:00 CET** \
Agenda:
- Michael Plagge, Florent Zara (Eclipse Foundation) - Update on the OSPO Alliance (10min).
- Camille Moulin and Benjamin Jean (InnoCube) - Hermine project: Open source code and data for legal compliance \
  [View slides](/resources/onramp_20220916/20220916_OSPO_OnRamp_Hermine_v03.pdf)
- Open discussion (*) (30min).

****

**Friday, June 17th, 10:30-12:00 CET** \
Agenda:
- Michael Plagge, Boris Baldassari (Eclipse Foundation) - Update on the OSPO Alliance (10min).
- Lucian Balea (RTE) - Setting up an OSPO at a power utility \
  [View slides](/resources/onramp_20220617/220617_OSPO_OnRamp_Building_an_OSPO_at_RTE.pdf) - [Watch video](https://peertube.xwiki.com/w/bDWqEvU5twosD2J1jpYj3f)
- Open discussion (*) (30min).

****

**Friday, May 20th, 10:30-12:00 CET** \
Agenda:
- Michael Plagge, Boris Baldassari (Eclipse Foundation) - Update on the OSPO Alliance (10min).
- Josep Prat (Aiven) - Building an OSPO: 3rd Party Outbound OSPO (35mn)\
  [View slides](https://jlprat.github.io/3rd-party-outbound-ospo/index.html) - [Watch video](https://peertube.xwiki.com/w/jXnZBWsiZ4spFaD6aawxfw)
- Open discussion (*) (30min).

****

**Friday, March 18th, 10:30-12:00 CET** \
Agenda:
- Michael Plagge, Boris Baldassari (Eclipse Foundation) - Update on the OSPO Alliance (10min).
- Daniel Izquierdo (Bitergia) - Establishing metrics early as an integral part of your OSPO first steps (35mn)\
  [Download slides](/resources/onramp_20220318/20220318_OSPO_OnRamp_Establishing_metrics_OSPO_first_steps.pdf) - [Watch video](https://peertube.xwiki.com/w/dwt4WL7AXvBVnfyRgdfpaw)
- Open discussion (*) (30min).

****

**Friday, February 18th, 10:30-12:00 CET** \
Agenda:
- Michael Plagge, Gaël Blondelle (Eclipse Foundation) - Update on the OSPO Alliance (10min).
- Carsten Emde (OSADL) - How to facilitate Open Source software licensing and get it right? (35mn)
- Open discussion (*) (30min).

****

**Friday, January 14th, 10:30-12:00 CET**  \
Agenda:
- Michael Plagge, Gaël Blondelle (Eclipse Foundation) - Update on the OSPO Alliance (10min).
- Henrik Plate (SAP) - [Manage Log4Shell and other open-source vulnerabilities with Eclipse Steady](/resources/onramp_20220114/20220114_OSPO_Alliance_Log4Shell.pdf) (35min).
- Open discussion (*) (30min).

****

**Friday, December 10th, 10:30-12:00 CET** \
Agenda:
- Michael Plagge (Eclipse Foundation) [Intro into the OSPO OnRamp concept](/resources/onramp_20211210/20211210_OSPO_OnRamp.pdf) - Call for active participation (10min).
- Michael Picht (SAP) - [The SAP OSPO](/resources/onramp_20211210/20211210_ext_ospo_onramp_sap_ospo_overview.pdf) (25min).
- Cédric Thomas (OW2) - [The OW2 OSS Good Governance initiative, an OSPO implementation blueprint](/resources/onramp_20211210/20211210_onramp-ggi-introduction-cedricthomas-CC-BY.pdf) (25min).
- Open discussion (*) (30min).
