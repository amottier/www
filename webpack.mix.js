/********************************************************************************
 * Copyright (c) 2021 Eclipse Foundation, OW2, OpenForum Europe and others 
 * 
 * This website and the accompanying materials are made available under the
 * terms of the Creative Commons Attribution 4.0 (International) License which
 * is available at https://creativecommons.org/licenses/by/4.0/.
 * 
 * SPDX-License-Identifier: CC-BY-4.0
 ********************************************************************************/

require('./node_modules/eclipsefdn-solstice-assets/webpack-solstice-assets.mix.js');
let mix = require('laravel-mix');
mix.EclipseFdnSolsticeAssets();

mix.setPublicPath('static');
mix.setResourceRoot('../');

mix.less('./less/styles.less', 'static/css/styles.css');
mix.js('js/main.js', './static/js/solstice.js')